// combine two booleans to form 1 boolean

// OR  ==> ||
true   ||   true  // true
true   ||   false // true
false  ||   true  // true
false  ||   false // false 

// AND ==> &&
true   &&   true  // true
true   &&   false // false
false  &&   true  // false
false  &&   false // false


6 < 8 && 7 === 7;
 true  &&   true
      true


6 > 8 || 7 === 7;
false ||  true
     true