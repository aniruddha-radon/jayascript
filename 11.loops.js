// for
// -- traditional

// initial value ==> from where the looping should start
// condition     ==> which condition stops the loop/iterations
// post iteration statement ==> what should happen after each iteration

for(let counter = 0; counter < 100; counter++) {
    // iteration block
    console.log(counter);
}

const players = ["Messi", "Schumacher", "Chopra", "Tiger Woods", "LeBron", "Federer"];

for(let index = 0; index < players.length; index++) {
    const player = players[index];
    console.log(player);
}

for(let index in players) {
    const player = players[index];
    console.log(player);
}

for(let player of players) {
    console.log(player);
}

const address = {
    city: "Poonah",
    state: "MH",
    zipcode: 411004,
    country: "India"
};

const keys = Object.keys(address); // keys cha array
// ["city", "state", "zipcode", "country"]
keys[0]; // "city"
for(let index = 0; index < keys.length; index++) {
    const key = keys[index];
    const value = address[key];
    console.log(value);
}

for(let key in address) {
    const value = address[key];
    console.log(value);
}


// -- for in ==> OBJECT KIVHA ARRAY
// array  ==> index based
// object ==> key based

// -- for of ==> ARRAY

const persons = [
    [10, "A", "OCCUPATION"], // element at index 0 is an array
    [20, "B", "CHUNGU"], // element at index 1 is an array
    [30, "C", "SGHHD"] // element at index 2 is an array
];

for(let index = 0; index < persons.length; index++) {
    
    // console.log(persons[index][0]); ==> EKA PHATKYAT


    const person = persons[index];  // element at index is an array
    const age = person[0];
    console.log(age);
}

for(let index in persons) {
    const person = persons[index];  // element at index is an array
    const age = person[0];
    console.log(age);
}

for(let person of persons) {
    const age = person[0];
    console.log(age);
}

// [20, 69, 71, 101, 121, 0, 5];
const findHighestNumber = (numbers) => {
    // assume ki 1st number in array is highest
    let highest = numbers[0]; // 20

    for(let index = 1; index < numbers.length; index++) {

        // if number at current index is higher than highest it is highest.
        if(numbers[index] > highest) {
            highest = numbers[index];
        }
    }

    return highest;
}