const num1 = 10;
const num2 = 2;

// addition +
// num1 + num2;
const addition = num1 + num2;

// subtraction -
// num1 - num2;
const difference = num1 - num2;

// multiplication *
// num1 * num2;
const product = num1 * num2;

// division /
// num1 / num2;
const quotient = num1 / num2;

// remainder (mod) %
// num1 % num2;
const remainder = num1 % 3; // 1

// power **
// num1 ** num2;
const power = num1 ** num2;