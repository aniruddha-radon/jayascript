// array of numbers
// find first prime number 

// [79, 105, 212, 436, 11]
// loop over the array of numbers
// for each number
//   -- check if number is prime
//      -- number is less than 4 return number
//      -- if number is divisible by 2 continue
//      
//   -- if number is prime, return it
//   -- else continue with the loop

const getFirstPrimeNumber = (numbers) => {
    for (let number of numbers) {
        console.log('CHECKING FOR NUMBER: ', number)
        let isPrime = true;

        if (number < 4) {
            console.log('NUMBER IS LESS THAN 4, returning...')
            return number; // number is either 1, 2 or 3
        }

        // if number is even don't check anything go to next number
        if (number % 2 === 0) {
            console.log('NUMBER IS DIVISIBLE BY 2. GOING TO NEXT NUMBER...')
            continue;
        }

        // 23 ===> number
        // check if divisible by
        // 3, 5, 7, 9, 11, 13, 15, 17, 19, 21 
        for (let i = 3; i < number / 2; i += 2) {
            console.log(`CHECKING IF ${number} is divisible by ${i}`);
            if (number % i === 0) {
                console.log(`${number} is divisible by ${i}. NOT PRIME. GOING TO NEXT NUMBER...`);
                isPrime = false;
                break;
            }
        }


        if (isPrime) {
            console.log(`NUMBER WAS NOT DIVISBLE BY ANY NUMBER, RETURNING.`);
            return number;
        }
    }
}

getFirstPrimeNumber([105, 212, 436, 11, 79]);
