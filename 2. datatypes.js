// types of data/values which can be stored in a variable/const 

// c code
int b = 5; // strong & static

// python code
number = 5; // loosely typed, static

// JS is loosely typed, dynamic language
let num = 5; // integer is stored.
num = 5.22; // float is stored.

// Number
// integer, floats, doubles, negatives

const integer = 10;
const decimal = 10.25;
const double = 123456780.129873;
const negative = -10;

typeof integer; // number
console.log(typeof decimal); // number

// String
// a-z, A-Z, 0-9, ~!@#$%^&*(_)
// enclosed in in "", '', or ``

const doubleQuotedString = "Mala bhook laglie!";
const singleQuotedString = '100 vela sangitla ahe!!';
const backTickedString = `1 var pankha laav`;

console.log(typeof backTickedString); // string


// Boolean
// true / false
const isRaining = false;
const isJevanTayar = true;

// conditions -- if else
// comparison operator -- output is boolean
// logical operator -- combine 2 booleans (&& ani ||)

console.log(typeof isJevanTayar); // boolean

// undefined
const undef = undefined; // DONT USE IN REAL LIFE CODE.

console.log(typeof undef); // undefined

// null --> pseudo datatype
const nullable = null;

console.log(typeof nullable); // object

// collection datatypes

// array
// Homogenous array ==> ekach prakarche elements
// Heterogenous arrays ==> assorted prakarche elements
// elements, seperated by commas
// enclosed in []

const numbers = [1, 2, 3, 4, 5];
const strings = ["Jay", "Ameya", "Avinav"];
const booleans = [true, false, false, true, true];
const arrays = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
];

const assorted = [
    10, 
    "Ash", 
    "Ketchum", 
    true, 
    undefined, 
    null, 
    [1, 2, 3], 
    { name: "abcd" }, 
    function () { console.log('inside an array') }
];

console.log(typeof assorted); // object

// object
// key: value pair, separated by commas
// enclosed in {}

const person = {
    age: 10,
    name: "Ash",
    surname: "Ketchum",
    isChampion: true,
    hasGirlfriend: undefined,
    hasBrain: null,
    badges: ["Pewter", "Fuchsia", "Goldenrod"],
    address: {
        city: "Pallet Town",
        county: "Kanto"
    },
    speak: function () {
        console.log("blah blah");
    }
};

console.log(typeof person); // object

// HW -> what are first class citizens?
// function 
function dummy() {
    console.log('this is a function');
}

console.log(typeof dummy); // function
