// compare LHS to RHS and return a boolean

// == equality 
// does NOT check datatype
8 == 8; // true
8 == 9; // false
8 == 7; // false
8 == "8"; // true
8 == "eight"; // false
8 == "a"; // false

// != inequality
8 != 8; // false
8 != 9; // true
8 != 7; // true
8 != "8"; // false
8 != "eight"; // true
8 != "a"; // true

// >  greater
8 > 8; // false
8 > 9; // false
8 > 7; // true
8 > "8"; // false
8 > "eight"; // false
8 > "a"; // false

// >= greater or equal
8 >= 8; // true
8 >= 9; // false
8 >= 7; // true
8 >= "8"; // true
8 >= "eight"; // false
8 >= "a"; // false

// <  less
8 < 8; // false
8 < 9; // true
8 < 7; // false
8 < "8"; // false
8 < "eight"; // false
8 < "a"; // false

// <= less or equal
8 <= 8; // true
8 <= 9; // true
8 <= 7; // false
8 <= "8"; // true
8 <= "eight"; // false
8 <= "a"; // false

// JS specific
// === equality
// checks BOTH value & datatype
8 === 8; // true
8 === "8"; // false

// !== inequality
8 !== "8"; // true ==> is value NOT equal to value
8 !== 8;   // false