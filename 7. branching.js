// if else if else
// switch

// if(<condition>) {
//     // if condition evaluates to true
//     // execute this block.    
// }

const age = 27;
const minimumAge = 35;

if(age <= minimumAge && 0) {
    console.log('ALLOWED');
}

// truthy & falsy
// 0, "", null, undefined, NaN, false ===> false

// IF block can be standalone
// it does not require else if or else

// else if, else blocks are dependent on if
// and are NOT standalone

// else if block does not require else
// you can have as many else if blocks



const patronAge = 17;

if(patronAge >= 25) {
    console.log('ALLOW AND SERVE ALCOHOL');
} else if (patronAge >= 18) {
    console.log('ALLOW, BUT DONT SERVE ALCOHOL');
} else {
    console.log('KHALI, haan re ek fight');
}

// only ONE block per if else if(s) and else is going to execute
// EVEN IF 2 or more conditions evaluate to true

// switch
// matches an input with all the cases

let pokemon = '8';

switch(pokemon) {
    case 8:
        console.log('aath');
        break;
    case 'pikachu':
        console.log('ELECTRIC POKEMON');
        break;
    case 'charizard':
        console.log('FIRE POKEMON');
        break;
    case 'bulbasaur':
        console.log('GRASS POKEMON');
        break;
    default:
        console.log('not available');
}