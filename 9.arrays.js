// arrays?
// elements, separated by commas
// enclosed in []

// numbers references mem 1000
const numbers = [5, 10, 15, 20, 25];
// index         0   1   2   3   4

// access an element
numbers[3]; // 20
const five = numbers[0];

// modify an element
// are we modifying the value of the reference? NO.
// reference madhe asleli value modify kartoy.
numbers[3] = 21;

// modifying the reference value stored in numbers.
// numbers = [10, 15]; // will not work.

// property
const length = numbers.length; // 5

// [5, 10, 15, 21, 25];
// methods

// add an element in array

// from rear
numbers.push(30); // [5, 10, 15, 21, 25, 30 <-- NEW];

// from start
numbers.unshift(0); // [0 <-- NEW, 5, 10, 15, 21, 25, 30];

// adding in middle
numbers.splice(4, 0, 20); // [0, 5, 10, 15, 20 <-- NEW, 21, 25, 30];


// remove an element from array
numbers.pop(); // [0, 5, 10, 15, 20, 21, 25]; --> removed 30

numbers.shift(); // [5, 10, 15, 20, 21, 25]; --> removed 0

// start at index 4 and delete 1.
numbers.splice(4, 1); // [5, 10, 15, 20, 25]; --> removed 21

// find the first occuring index of an element
numbers.findIndex(15); // 2
numbers.findIndex(400); // -1

// map, filter & reduce
// map --> creates a new array and stores the modified elements

const primeNumbers = [2, 3, 5, 7, 11, 13];
// expected output ==> [4, 9, 25, 49, 121, 169];
// primeNumbers remains untouched.
const square = x => x ** 2;

square(primeNumbers[2]); // 25
square(primeNumbers[0]); // 4

const squareOfPrimes = primeNumbers.map(square);
const cubeOfPrimes = primeNumbers.map(x => x ** 3);

const strings = ["bhadvya", "bhendichya", "ghantush"];
const capitalizedShivi = strings.map(e => e.toUpperCase());

// filter
const persons = [
    { name: 'Jay', age: 32 },
    { name: 'Avinav', age: 28 },
    { name: 'Someya', age: 36 },
    { name: 'Gabs', age: 15 },
    { name: 'Shreya', age: 27 },
    { name: 'Aniruddha', age: 31 }
];

const peopleOver30 = persons.filter(p => p.age >= 30);

// destructuring

const cutlery = ["spoon", "fork", "knife", "spork"];

// const spoon = cutlery[0];
// const fork = cutlery[1];
// const knife = cutlery[2];
// const spork = cutlery[3];

const [spoon, fork, knife, spork] = cutlery;

const person = [10, "baburao", "apte", 20000];
const [age, name, surname, salary] = person;

const somethingComplicated = [10, () => console.log('value changed')];
const [value] = somethingComplicated;

const [, fn] = somethingComplicated;
fn();

const names = ['Ameya', 'Pratik', 'Jay', 'Avin', 'Gabs', 'Renu', 'Shreya'];
const [,,,,gabs] = names;

// spread operator ...
const bodyParts = ["eyes", "nose", "mouth", "ears", "neck", "fingers"];
const bp = bodyParts; // copies the reference into bp
// bp & bodyParts point to the same array.

bodyParts.push("thumb"); // ["eyes", "nose", "mouth", "ears", "neck", "fingers", "thumb"];
bp.push("knees"); //  ["eyes", "nose", "mouth", "ears", "neck", "fingers", "thumb", "knees"];

const bpClone = [ ...bodyParts ]; // creates a NEW array and copies the elements of bodyParts
bpClone.push("bhuvaya"); // does NOT modify the bodyParts array.

// rest operator ...

const [, , , , ...belowFace] = bodyParts;
// creates an array of all the rest elements in belowFace
