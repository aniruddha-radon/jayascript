// concatenation/merging/joining
// old way +
const title = "Harry Potter";
const author = "J. K. Rowling";

let fullTitle = title + ", " + author; // Harry Potter, J. K. Rowling

// new way (ES6+)
fullTitle = `${title}, ${author}`; // Harry Potter, J. K. Rowling

// property
title.length; // 12

// methods
title.toUpperCase(); // HARRY POTTER
title.toLowerCase(); // harry potter

// does not replace in memory (returns new value)
title.replace("P", "H"); // Harry Hotter;

// immutability
let name = "Jay Lonkar";
name = "Aniruddha Gohad"; // ALLOWED.

let word = "Nose";
word[0] = "P"; // NOT ALLOWED.

// https://javascript.info/
// https://javascript.info/string


// reverse the words in a string

// str == about me
const reverseWords = str => (str.split(" ").reverse()).join(" "); // ["about", "me"];