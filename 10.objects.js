const person = {
    name: 'Jay',
    surname: 'Lonkar',
    age: 29,
    address: {
        city: "Pune",
        state: "MH"
    },
    isMarried: false,
    employmentHistory: ['Pratham', 'Google', 'Facebook'],
    speak: () => console.log('Hello, my name is SEEEEEMMAAAA'),
    code: function () { console.log('TAP TAP TAP... project complete') },
}

// how to access value
// dot operator
person.name; // 'Jay'
person.address; // { city: "Pune", state: "MH" }

// square bracket
// objectName['keyName'];
person['surname']; // 'Lonkar'
person['code']; // function () { console.log('TAP TAP TAP... project complete') }

const key = 'isMarried';
person[key]; // false

person.key; // undefined. why? key is not a key in the person object

// modify values
person.age = 28;
person['isMarried'] = true;

const changeKey = 'code';
person[changeKey] = () => console.log('TAP TAP TAP... nako');


// destructuring
// const name = person.name;
// const surname = person.surname;
// const age = person.age;

const { name, surname, age } = person;

const { batataWada } = person; // undefined

// spread operator
const pClone = person; // copies the reference into pClone
pClone.batataWada = 'garam';

const personClone = { ...person };

// rest operator
const { name, surname, ...bakichaJay } = person;