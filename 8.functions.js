// reusable block of code (which maybe named)
// which can be invoked
// and which can accept an argument/parameters
// and which may return some data/value

// 2 main types of functions
// 1. named
// 2. anonymous / function expression
//  a. --> function keyword
//  b. --> arrow function

// named
// function keyword

// function are always verbs (kriyavachak)

//keyword  fn name (arguments list)
function   callJay () {
    // block/scope of function
    // code here...
}

// non parameterized, non (explicitly) returning
function greet() {
    console.log('HELLOOOOOO, JAY!!');
}

greet;  // referencing the function
greet();// invoke/call the function

// parameterized, non (explicitly) returning
function greetUser(userName) {
    console.log(`HELLOO, ${userName}`);
}

greetUser('Jay');

function greetMultipleUsers(u1, u2, u3, u4) {
    console.log(`HELLO EVERYONE (${u1} ${u2} ${u3} ${u4})`);
}

greetMultipleUsers('a', 'b', 'c', 'd');


// non parameterized, returning
function getGreeting() {
    return 'Helloooo!';
}

const greeting = getGreeting();
console.log(greeting);

function callJayAndSpeak() {
    console.log('Dialing...');

    return 'Hello, Me aniruddha';
    // STOPS THE EXECUTION OF THE FUNCTION.
    console.log('Waiting for jay to speak');
}

callJayAndSpeak();

// parameterized, returning
function add(num1, num2) {
    return num1 + num2;
}

const addition = add(5, 7);


// 2 anonymous functions
// a) function keyword vaprun kelele fn expressions

// fn expression cha reference add madhe store kartoy
const add = function (num1, num2) {
    return num1 + num2;
}

const sum = add(7, 5);

// b) function keyword NA vaparta kelele fn expression
// FAT ARROW FUNCTION
const subtract = (num1, num2) => {
    return num1 - num2;
}

// does not require the return statement & block {}
// if fn is of just 1 line
const multiply = (num1, num2) => num1 * num2;

const product = multiply(9, 19);

// does not require paranthesis for parameters if ther is only 1 parameter
const drinkBeverage = beverage => console.log(`${beverage}, gulp gulp gulp`);

drinkBeverage('Beer');


const greet = (name) => {
    if(name === "Johnny") 
        return 'Hello, my love';
    
    return `Hello, ${name}`;
}