// a => variable name
// = => assignment operator, puts the value in the variable
// 5 => value/literal
a = 5; // BAD PRACTICE.

// var --> AVOID. nakoch vaprus
var number = 10; // declaration + assignment
// keyword var is required only while declaring
// is called variable bcoz its value can vary/change
number = 15; // reassign the value

var primeNumber; // declaration

primeNumber = 7; // assignment

// let --> hech vapraycha ahe
let num = 5;

let primeNum;

primeNum = 11;

primeNum = 13;

// constants
// const --> PREFER const over let
let age = 29; // can change, karan ki variable ahe.

// const are always declaration + assignment
const PI = 3.14;

const address; // INVALID
address = "Brahma Chaitanya";

// declared variables have the value undefined.